﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace TalkGovToMeSynth.Controllers
{
    public class ValuesController : ApiController
    {
        private static Stream _stream = null;
        private static MemoryStream _soundStream = null;
        private static HttpResponseMessage result;
        private static Authentication _auth;

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public HttpResponseMessage Get(string text)
        {
            _soundStream = new MemoryStream();

            if (text == null)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            else if (text.Length < 4)
                return new HttpResponseMessage(HttpStatusCode.OK);

            Console.WriteLine("Starting Authtentication");
            AccessTokenInfo token;
            Authentication auth = null;

            if (_auth == null)
            {
                auth = Auth();
            }

            if (auth != null)
            {
                try
                {
                    token = auth.GetAccessToken();
                    Console.WriteLine("Token: {0}\n", token.access_token);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed authentication.");
                    Console.WriteLine(ex.ToString());
                    Console.WriteLine(ex.Message);
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }

                Console.WriteLine("Starting TTSSample request code execution.");

                string requestUri = "https://speech.platform.bing.com/synthesize";

                var cortana = new Synthesize(new Synthesize.InputOptions()
                {
                    RequestUri = new Uri(requestUri),
                    // Text to be spoken.
                    Text = text,
                    VoiceType = Gender.Female,
                    // Refer to the documentation for complete list of supported locales.
                    Locale = "en-US",
                    // You can also customize the output voice. Refer to the documentation to view the different
                    // voices that the TTS service can output.
                    VoiceName = "Microsoft Server Speech Text to Speech Voice (en-US, ZiraRUS)",
                    // Service can return audio in different output format. 
                    OutputFormat = AudioOutputFormat.Riff16Khz16BitMonoPcm,
                    AuthorizationToken = "Bearer " + token.access_token,
                });

                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);

                cortana.OnAudioAvailable += (object sender, GenericEventArgs<Stream> args) =>
                {

                    //args.EventData.CopyTo(_soundStream);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    //SoundPlayer player = new SoundPlayer(args.EventData);
                    //player.PlaySync();


                    result.Content = new StreamContent(ReadFully(args.EventData));
                    args.EventData.Dispose();

                    //result.Content = new StreamContent(args.EventData);
                    result.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");
                };
                //cortana.OnAudioAvailable += PlayAudio;
                cortana.OnError += ErrorHandler;

                cortana.Speak(CancellationToken.None).Wait();

                return result;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }

        private Authentication Auth()
        {
            return new Authentication("TGTM", "abf68a9aebc14ed4bb32ebf0b93891cd");
        }

        public static MemoryStream ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return new MemoryStream(ms.ToArray());
            }
        }

        /// <summary>
        /// This method is called once the audio returned from the service.
        /// It will then attempt to play that audio file.
        /// Note that the playback will fail if the output audio format is not pcm encoded.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="GenericEventArgs{Stream}"/> instance containing the event data.</param>
        static void PlayAudio(object sender, GenericEventArgs<Stream> args)
        {
            Console.WriteLine(args.EventData);

            args.EventData.CopyTo(_soundStream);

            // For SoundPlayer to be able to play the wav file, it has to be encoded in PCM.
            // Use output audio format AudioOutputFormat.Riff16Khz16BitMonoPcm to do that.
            _stream = args.EventData;
            //SoundPlayer player = new SoundPlayer(args.EventData);
            //player.PlaySync();
            //args.EventData.Dispose();
        }

        /// <summary>
        /// Handler an error when a TTS request failed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GenericEventArgs{Exception}"/> instance containing the event data.</param>
        static void ErrorHandler(object sender, GenericEventArgs<Exception> e)
        {
            Console.WriteLine("Unable to complete the TTS request: [{0}]", e.ToString());
        }
    }
}
